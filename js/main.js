

/**
 * Main AngularJS Web Application
 */
var app = angular.module('angula', [
  'ngRoute', 'ui.bootstrap', 'ngAnimate', 'ngMaterial'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", { templateUrl: "partials/home.html", controller: "HomeCtrl" })
    .otherwise("/404", {templateUrl: "partials/404.html", controller: "PageCtrl"});
}]);

app.controller('HomeCtrl', function ($scope,$http, $interval) {
	$scope.urlDepth=0;
	$scope.loadingTime=false;
	$scope.invalidUrl=false;
	$scope.errorMessage=null;
	$scope.displayData=false;
	
	$scope.webCrawlFunc=function(){
		$scope.loadingTime=true;
		$scope.invalidUrl=false;
		$scope.displayData=false;
		$scope.errorMessage= "Please wait !! we are crawling websites...";
		console.log("urllll",$scope.urlLink);
		$http.get("http://localhost:8080/homePageApi/fetchWebCrawling/?format=json",{
			params:{'urlLink':$scope.urlLink,'urlDepth':$scope.urlDepth}
		})
		.success(function(response) {
			console.log(response);
			if(response =='error'){
				$scope.invalidUrl=true;
				$scope.loadingTime=false;
				$scope.errorMessage=null;
				$scope.displayData=false;
			}
			else{
				$scope.outputData=response;
				$scope.loadingTime=false;
				$scope.errorMessage=null;
				$scope.displayData=true;
			}
		}).error(function (error, status){
			$scope.invalidUrl=true;
			$scope.loadingTime=false;
			$scope.errorMessage=null;
			$scope.displayData=false;
		  }); 
	}
	

});


